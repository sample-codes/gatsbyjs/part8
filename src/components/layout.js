import React from "react"
import { css } from "@emotion/core"
import { useStaticQuery, Link, graphql } from "gatsby"
import {Helmet} from "react-helmet";

import { rhythm } from "../utils/typography"
export default ({ children }) => {
  const data = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            title
          }
        }
      }
    `
  )
  return (
    <div
      css={css`
        margin: 0 auto;
        max-width: 700px;
        padding: ${rhythm(2)};
        padding-top: ${rhythm(1.5)};
      `}
    >
      <Helmet>
        <meta charSet="utf-8" />
        <title>My Title</title>
        <link rel="canonical" href="http://mysite.com/example" />
      </Helmet>
      <Link to={`/`}>
        <h3
          css={css`
            margin: ${rhythm(1)} ${rhythm(1)} ${rhythm(1)} 0;
            display: inline-block;
            font-style: normal;
          `}
        >
          {data.site.siteMetadata.title}
        </h3>
      </Link>
      <Link
        to={`/about/`}
        css={css`
          float: right;
          margin: ${rhythm(1)};
          display: block;
        `}
      >
        About
      </Link>
      <Link
        to={`/my-files/`}
        css={css`
          float: right;
          margin: ${rhythm(1)};
          display: block;
        `}
      >
        My Files
      </Link>
      
      {children}
    </div>
  )
}